from flask import Flask, jsonify, request
from datetime import datetime
import logging
from os import environ 

__author__ = "Nikita Korepanov"

logger = logging.getLogger("MainApp")
logger.setLevel(logging.INFO)

fh = logging.FileHandler("app_lg.log", mode='w')
 
formatter = logging.Formatter('%(asctime)s - %(funcName)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

app = Flask(__name__)

def process1(request):
    logger.info('process1 in progress')

def process2(request):
    logger.info('process2 in progress')
    if ('notawaiting' in request.values.to_dict()) and (request.values.to_dict()['notawaiting'] == '1'):
        logger.error('Invalid parametr "notawaiting=1"')
        return 'invalid param p2'
    return 'valid param p2'

def process3(request):
    logger.info('process3 in progress')

@app.route('/api', methods=['GET', 'POST', 'PUT', 'DELETE'])
def api_func():
    ''' логируем что пришло '''
    time = datetime.utcnow() # логгирование времени
    method = request.method # логгирование метода
    url = request.url # логгирование url 
    parameters = request.values.to_dict() # логгирование параметров
    logger.info('Method: {} - URL: {} - Params: {}'.format(method, url, parameters))

    if request.method == 'GET':
        if ('invalid' in request.values.to_dict()) and (request.values.to_dict()['invalid'] == '1'):
            logger.error('Invalid parametr "invalid=1"')
            response = 'invalid param "invalid=1"'
        else:
            response = 'all params were valid'
    else: # логгировать и валидировать как ошибку
        logger.error('Method not GET')
        response = 'wrong method'

    logger.info('Start process1')
    p1 = process1(request)
    logger.info('process1 is completed')
    logger.info('Start process2')
    p2 = process2(request)
    logger.info('process2 is completed')
    logger.info('Start process3')
    p3 = process3(request)
    logger.info('process3 is completed')
    response += ' ' + p2
    return response, 200

@app.errorhandler(404)
def page_not_found(e):
    method = request.method # логгирование метода
    url = request.url # логгирование url 
    parameters = request.values.to_dict() # логгирование параметров
    logger.error('Method: {} - URL: {} - Params: {} - Wrong way!'.format(method, url, parameters))
    return 'Wrong way', 200

if __name__ == '__main__':
    PORT = environ.get('PORT')
    app.run(port=PORT, debug=True)