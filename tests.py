import unittest

from simple_app import app

class FlaskAppTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_GET_empty(self):
        resp = self.app.get('/')
        resp = resp.data
        assert 'Wrong way' in resp.decode()

    def test_GET_another_path(self):
        resp = self.app.get('/something/wrong')
        resp = resp.data
        assert 'Wrong way' in resp.decode()

    def test_GET_another_path_with_params(self):
        resp = self.app.get('/something?a=1&b=2')
        resp = resp.data
        assert 'Wrong way' in resp.decode()

    def test_POST(self):
        resp = self.app.post('/api')
        resp = resp.data
        assert 'wrong method' in resp.decode()

    def test_GET_invalid_param(self):
        resp = self.app.get('/api?invalid=1')
        resp = resp.data
        assert 'invalid param "invalid=1"' in resp.decode()
    
    def test_GET_valid_param(self):
        resp = self.app.get('/api?a=1&b=4&c=44')
        resp = resp.data
        assert 'all params were valid' in resp.decode()

    def test_GET_valid_param_1(self):
        resp = self.app.get('/api?invalid=11')
        resp = resp.data
        assert 'all params were valid' in resp.decode()

    def test_GET_valid_param_2(self):
        resp = self.app.get('/api?invalib=1')
        resp = resp.data
        assert 'all params were valid' in resp.decode()

    def test_GET_invalid_param_proc_2(self):
        resp = self.app.get('/api?notawaiting=1')
        resp = resp.data
        assert 'invalid param p2' in resp.decode()

    def test_GET_valid_param_proc_2(self):
        resp = self.app.get('/api?smth=anyth')
        resp = resp.data
        assert 'valid param p2' in resp.decode()

if __name__ == '__main__':
    unittest.main()